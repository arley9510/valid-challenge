# Valid challenge

In this file I'm gonna explain the architecture and the implementation of my software solution.

## Development tools

* [node js](https://nodejs.org/es/)
* [yarn](https://yarnpkg.com/es-ES/)
* [axios](https://github.com/axios/axios)
* [redux](https://es.redux.js.org/)
* [react-redux](https://github.com/reduxjs/react-redux)
* [redux-thunk](https://github.com/reduxjs/redux-thunk)
* [redux-thunk](https://github.com/reduxjs/redux-thunk)
* [redux-logger](https://github.com/evgenyrodionov/redux-logger)
* [react navigation](https://reactnavigation.org/)
* [react native elements](https://github.com/react-native-training/react-native-elements)
* [jest](https://jestjs.io/)
* [git](https://git-scm.com/)

## Project info and IDEs
```
  React Native Environment Info:
    System:
      OS: Windows 10
      CPU: x64 Intel(R) Core(TM) i5-4690K CPU @ 3.50GHz
      Memory: 6.79 GB / 15.95 GB
    Binaries:
      Yarn: 1.9.4 - C:\Program Files (x86)\Yarn\bin\yarn.CMD
      npm: 5.6.0 - C:\Program Files\nodejs\npm.CMD
    IDEs:
      Android Studio: Version  3.1.0.0 AI-173.4907809
```

## File structure
```
project
├── android
│
├── ios
│
├── src
│      │
│      ├── components
│      │              │
│      │              ├── detail
│      │              │         │  Detail.js
│      │              │         │  DetailStyle.js
│      │              │         └───
│      │              ├── favorites
│      │              │             │ Favorites.js
│      │              │             │ FavoritesStyle.js
│      │              │             └───
│      │              ├── search
│      │              │         │  ListStile.js
│      │              │         │  Search.js
│      │              │         │   SearchList.js
│      │              │         └───
│      │              └───
│      │ 
│      ├── enviroments
│      │              │  Environment.js
│      │              └───
│      │
│      ├── redux
│      │        ├── actions
│      │        │           │   ActionTypes.js
│      │        │           │   index.js
│      │        │           └───
│      │        ├── reducers
│      │        │           │   SearchReducer.js
│      │        │           └───
│      │        │  ConfigureStore.js
│      │        └───
│      │
│      ├── services
│      │           ├── asyncStorage
│      │           │               │   AsyncStorageService.js
│      │           │               └───
│      │           │
│      │           ├── interceptor
│      │           │              │   HttpClient.js
│      │           │              └───
│      │           │  
│      │           ├── navigation
│      │           │             │   NavigationService.js
│      │           │             └───
│      │           │  
│      │           └───
│      │   Routes.js
│      └───
│   .babelrc
│   .buckconfig
│   .flowconfig
│   .gitattributes
│   .gitignore
│   .watchmanconfig
│   App.js
│   app.json
│   index.js
│   package.json
│   README.md
│   yarn.lock
└───
```

## Project classes
 
 * src/components/
    
    - public Detail
    - public Favorites
    - public Search
    - public SearchList

## Project implementation

* clone the project whit this command 

```
    clone https://arley9510@bitbucket.org/arley9510/valid-challenge.git
```

* install libraries and dependencies in the project folder in a terminal

```
    yarn install
```
 
* Run the project the react native command line

```
    react-native run-android
```
