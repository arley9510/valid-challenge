/**
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {Provider} from 'react-redux';

import Routes from './src/Routes';
import ConfigureStore from './src/redux/ConfigureStore';
import NavigationService from './src/services/navigation/NavigationService';

const store = ConfigureStore();

export default class App extends Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <Routes
                    ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                    }}
                />
            </Provider>
        );
    }
}
