/**
 * @format
 * @flow
 */
import * as ActionTypes from '../actions/ActionTypes';

const SearchReducer = (state = {
    isLoading: true,
    errorMessage: null,
    searchResp: []
}, action) => {
    if (action.type === ActionTypes.ADD_SEARCH) {
        return {...state, isLoading: false, errorMessage: null, searchResp: action.searchResp};
    }
    if (action.type === ActionTypes.SEARCH_LOADING) {
        return {...state, isLoading: true, errorMessage: null, searchResp: []};
    }
    if (action.type === ActionTypes.SEARCH_FAILED) {
        return {...state, isLoading: false, errorMessage: action.error, searchResp: []}
    }

    return state;
};

export default SearchReducer;
