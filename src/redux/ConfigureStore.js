/**
 * @format
 * @flow
 */
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {createStore, combineReducers, applyMiddleware} from 'redux';

import SearchReducer from './reducers/SearchReducer';

const configureStore = () => {
    return createStore(
        combineReducers({
            search: SearchReducer
        }),
        applyMiddleware(thunk, logger)
    );
};

export default configureStore;
