/**
 * @format
 * @flow
 */

// Search types
export const ADD_SEARCH = 'ADD_SEARCH';
export const SEARCH_FAILED = 'SEARCH_FAILED';
export const SEARCH_LOADING = 'SEARCH_LOADING';
