import {HttpClient} from '../../services/interceptor/HttpClient';
import Environment from '../../enviroments/Environment';
import {Alert} from 'react-native';

export function getSearch(query: string, page: string) {
    return (dispatch) => {
        const request = HttpClient
            .get('search/movie?api_key=' + Environment.API_KEY + '&query=' + query + '&page=' + page);

        return request.then(
            response => {
                if (response.data.total_results > 0) {
                    dispatch(setSearch(response.data))
                } else {
                    Alert.alert('No titleFound');
                }
            },
            error => dispatch(setSearchError(error))
        );
    }
}

export function setSearch(data) {
    return {
        type: "ADD_SEARCH",
        searchResp: data
    }
}

export function setSearchError(data) {
    return {
        type: "SEARCH_FAILED",
        error: data
    }
}
