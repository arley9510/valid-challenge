import axios from 'axios';
import join from 'url-join';
import {NetInfo, Alert} from 'react-native';

import Environment from './../../enviroments/Environment';

const absoluteUrlRegex = /^(?:\w+:)\/\//;

axios.interceptors.request.use(async (config) => {
    if (!absoluteUrlRegex.test(config.url)) {

        config.url = join(Environment.URL_BASE, config.url);
    }

    return config;
});

axios.interceptors.response.use(async (response) => {

    return response;
}, function (error) {

    NetInfo.getConnectionInfo().then((info) => {
        if (info.type === 'none') {
            Alert.alert('Error en la red', 'Conecta el dispositivo a internet');
        }
    });

    if (error.request.status === 422) {
        const test = JSON.parse(error.request._response);

        test.map(field => {
            Alert.alert('Error', field.message);
        });

        return
    }

    if (error.config.timeout === 0) {
        Alert.alert('Error en el servidor', 'Intentalo más tarde');
    }
});

export const HttpClient = axios;