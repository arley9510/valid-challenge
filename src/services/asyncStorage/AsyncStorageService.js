import {AsyncStorage, Alert} from 'react-native';

async function setItem(key, value) {
    try {
        return await AsyncStorage.setItem(key, value);
    } catch (e) {
        Alert.alert('Error', 'Se produjo un error inesperado');
    }
}

async function getItem(key) {
    try {
        return await AsyncStorage.getItem(key);
    } catch (e) {
        Alert.alert('Error', 'Se produjo un error inesperado');
    }
}

async function getAllItems() {
    try {
        return await AsyncStorage.getAllKeys();
    } catch (e) {
        Alert.alert('Error', 'Se produjo un error inesperado');
    }
}

async function clearItem(key) {
    try {
        return await AsyncStorage.removeItem(key);
    } catch (e) {
        Alert.alert('Error', 'Se produjo un error inesperado');
    }
}

export default {
    getAllItems,
    clearItem,
    setItem,
    getItem,
};