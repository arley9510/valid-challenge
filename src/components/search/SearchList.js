/**
 * @format
 * @flow
 */
import React, {PureComponent} from 'react';
import {Card} from 'react-native-elements';
import {FlatList, TouchableOpacity, View} from 'react-native';

import ListStile from './ListStile';
import NavigationService from '../../services/navigation/NavigationService';

export default class SearchList extends PureComponent {

    render() {
        if (this.props.list.searchResp.results !== null && typeof this.props.list.searchResp.results !== 'undefined') {

            return (
                <FlatList
                    contentContainerStyle={ListStile.list}
                    data={this.props.list.searchResp.results}
                    keyExtractor={item => item.id.toString()}
                    renderItem={({item}) =>
                        <TouchableOpacity onPress={() => NavigationService.navigate('detail', {data: item})}>
                            <Card
                                title={item.title}
                                image={{uri: 'https://image.tmdb.org/t/p/w500/' + item.poster_path}}>
                            </Card>
                        </TouchableOpacity>
                    }
                />
            );
        } else {
            return <View/>;
        }
    }
}
