/**
 * @format
 * @flow
 */
import {View} from 'react-native';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {SearchBar} from 'react-native-elements'

import SearchList from './SearchList';
import {getSearch} from '../../redux/actions';

const mapStateToProps = state => {
    return {
        search: state.search
    }
};

const mapDispatchToProps = dispatch => ({
    newSearch: (param1, param2) => dispatch(getSearch(param1, param2)),
});

class Search extends Component {

    constructor(props) {
        super(props);

        this.search = this.search.bind(this);
    }

    search(query: string) {
        if (query !== '') {
            this.props.newSearch(query, 1);
        }
    };

    render() {
        return (
            <View>
                <SearchBar
                    round
                    lightTheme
                    placeholder='Search'
                    onChangeText={this.search}
                />
                <SearchList list={this.props.search}/>
            </View>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
