import {StyleSheet} from 'react-native';

const FavoritesStyle = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
    },
    button: {
        marginTop: 5,
        marginHorizontal: 25,
    }
});

export default FavoritesStyle;
