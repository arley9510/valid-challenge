/**
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {Card} from "react-native-elements";
import {Alert, Button, FlatList, TouchableOpacity, View} from 'react-native';

import AsyncStorageService from "../../services/asyncStorage/AsyncStorageService";
import NavigationService from "../../services/navigation/NavigationService";
import FavoritesStyle from "./FavoritesStyle";

export default class Favorites extends Component {

    state = {
        data: [],
    };

    constructor(props) {
        super(props);

        this.getItems = this.getItems.bind(this);
    }

    componentDidMount() {
        this.getItems();
    }

    getItems() {
        this.setState({
            data: []
        });
        AsyncStorageService
            .getAllItems().then((response) => {
            response.map((item) => {
                if (item.toString().length > 0) {
                    AsyncStorageService.getItem(item)
                        .then((info) => {
                            this.setState({
                                data: this.state.data.concat([JSON.parse(info)])
                            });
                        });
                }
            });
        });
    }

    clearItem(item) {
        Alert.alert('Remove from favorites');
        AsyncStorageService.clearItem(item).then(() => {
            this.getItems();
        });
    }

    render() {
        if (this.state.data.toString().length > 0) {
            return (
                <View>
                    <FlatList data={this.state.data}
                              keyExtractor={item => item.id.toString()}
                              renderItem={({item}) =>
                                  <View style={FavoritesStyle.container}>
                                      <TouchableOpacity
                                          onPress={() => NavigationService.navigate('detail', {data: item})}>
                                          <Card
                                              title={item.title}
                                              image={{uri: 'https://image.tmdb.org/t/p/w500/' + item.poster_path}}>
                                          </Card>
                                      </TouchableOpacity>
                                      <View style={FavoritesStyle.button}>
                                      <Button title={'remove'}
                                              onPress={() => this.clearItem(item.original_title)}/>
                                      </View>
                                  </View>
                              }/>
                </View>
            );
        } else {
            return <View/>;
        }
    }
}
