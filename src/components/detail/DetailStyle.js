import {StyleSheet} from 'react-native';

const DetailStyle = StyleSheet.create({
    container: {
        flex: 1
    },
    text: {
        marginBottom: 10
    },
    button: {
        borderRadius: 0,
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 0
    },
    containerButton: {
        marginBottom: 20,
        marginTop: 20
    }
});

export default DetailStyle;
