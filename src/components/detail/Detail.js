/**
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {Button, Card} from 'react-native-elements';
import {Alert, ScrollView, Text, View} from 'react-native';

import DetailStyle from './DetailStyle';
import NavigationService from '../../services/navigation/NavigationService';
import AsyncStorageService from '../../services/asyncStorage/AsyncStorageService';

export default class Detail extends Component {

    constructor(props) {
        super(props);

        this.addFavorites = this.addFavorites.bind(this);
    }

    static navigationOptions(props) {
        const info = props.navigation.state.params;

        return {
            title: info.data.original_title,
        };
    }

    async addFavorites() {
        const data = this.props.navigation.state.params.data;
        AsyncStorageService
            .getItem(data.original_title)
            .then((response) => {
                if (response === null) {
                    AsyncStorageService
                        .setItem(data.original_title, JSON.stringify(data))
                        .then(() => {
                            Alert.alert('Added to Favorites')
                        });
                } else {
                    Alert.alert('Already in favorites')
                }
            });
    }

    render() {
        const {navigation} = this.props;
        const info = navigation.state.params.data;

        return (
            <ScrollView style={DetailStyle.container}>
                <View>
                    <Card
                        title={info.original_title}
                        image={{uri: 'https://image.tmdb.org/t/p/w500/' + info.backdrop_path}}>
                        <Text style={DetailStyle.text}>
                            {info.overview}
                        </Text>
                        <Button
                            backgroundColor='#03A9F4'
                            buttonStyle={DetailStyle.button}
                            title='Add to favorites'
                            onPress={this.addFavorites}/>
                    </Card>
                </View>
                <View style={DetailStyle.containerButton}>
                    <Button title='Back'
                            onPress={() => NavigationService.navigate('search')}/>
                </View>
            </ScrollView>
        );
    }
}
