/**
 * @format
 * @flow
 */
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {createStackNavigator, createSwitchNavigator, createBottomTabNavigator} from 'react-navigation';

import Search from './components/search/Search';
import Detail from './components/detail/Detail';
import Favorites from './components/favorites/Favorites';

const bottom = createBottomTabNavigator(
    {
        search: {
            screen: Search,
            navigationOptions: {
                tabBarIcon: ({focused, tintColor}) => {
                    return <Ionicons name="ios-search" size={30} color={tintColor}/>;
                }
            }
        },
        favorites: {
            screen: Favorites,
            navigationOptions: {
                tabBarIcon: ({focused, tintColor}) => {
                    return <Ionicons name="ios-star" size={30} color={tintColor}/>;
                }
            }
        }
    }, {
        initialRouteName: 'search',
        backBehavior: 'history'
    }
);

const stack = createStackNavigator({
    detail: Detail
}, {
    backBehavior: 'history'
});


export default createSwitchNavigator({
    bottom: bottom,
    stack: stack
}, {
    backBehavior: 'history',
    resetOnBlur: false,
    initialRouteName: 'bottom'
});